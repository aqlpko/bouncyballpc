#include "bouncyballpc.h"
#include <cmath>

BouncyBallPc::BouncyBallPc(float dt)
{

    update.setSingleShot(false);
    update.setInterval(dt*1000);
    connect(&update,SIGNAL(timeout()),this,SLOT(step()));
    m.display=true;
    m.ball=-1;
    m.world=-1;
    m.lvl=-1;
    m.mis=-1;

    b[0].mass=0.45f;
    b[0].friction=0.2f;
    b[0].restutition=0.6f;
    b[0].density=80.7f;
    b[0].r=0.11f;
    b[0].modelN=1;

    b[1].mass=0.05f;
    b[1].friction=0.1f;
    b[1].restutition=0.5f;
    b[1].density=1.5f;
    b[1].r=0.2f;
    b[1].modelN=3;

    b[2].mass=0.6f;
    b[2].friction=0.3f;
    b[2].restutition=0.8f;
    b[2].density=82.9f;
    b[2].r=0.12f;
    b[2].modelN=4;

    b[3].mass=0.28f;
    b[3].friction=0.2f;
    b[3].restutition=0.7f;
    b[3].density=66.8f;
    b[3].r=0.1f;
    b[3].modelN=5;

}

BouncyBallPc::~BouncyBallPc()
{

}

void BouncyBallPc::start()
{
    update.start();
}

void BouncyBallPc::step()
{
    emit newFrame(fingerPos,touch);
    menu();
    gameStep();
    setForce();
}
void BouncyBallPc::setForce()
{
    if(touch && !m.display)
    {
        float pos[2];
        emit getObjectPos(id,b[activeB].modelN,pos);

        float force[2];
        force[0]=(pos[0]-fingerPos[0])*1000.0f;
        force[1]=(pos[1]-fingerPos[1])*2000.0f;

        float r=b[activeB].r;
        if(fingerPos[0]>pos[0]-r && fingerPos[0]<pos[0]+r)
        {
            pos[1]-=r;
        }
        else
        {
            float d;
            if(fingerPos[0]>pos[0])
                d=(fingerPos[0]-pos[0]-r)/(0.6f-pos[0]-r);
            else
                d=(pos[0]-r-fingerPos[0])/(-0.6f-pos[0]+r);

            pos[0]+=d*r;
            pos[1]-=r*sqrt(1-d*d);

        }

        emit setForce(id,b[activeB].modelN,force,pos);

    }
}
bool BouncyBallPc::isCircleClicked(float *pos1, float *pos2, float r)
{
    float d1=pos1[0]-pos2[0];
    float d2=pos1[1]-pos2[1];
    float d=d1*d1+d2*d2;
    bool isClicked=false;
    if(d<r*r)
        isClicked=true;
    return isClicked;
}
bool BouncyBallPc::isRectangleClicked(float *pos1, float *pos2, float a,float b)
{
    bool w1=pos1[0]+a*0.5>pos2[0];
    bool w2=pos1[0]-a*0.5<pos2[0];
    bool w3=pos1[1]+b*0.5>pos2[1];
    bool w4=pos1[1]-b*0.5<pos2[1];

    if(pos1[0]+a*0.5>pos2[0] && pos1[0]-a*0.5<pos2[0] && pos1[1]+b*0.5>pos2[1] && pos1[1]-b*0.5<pos2[1])
        return true;
    else
        return false;
}
int BouncyBallPc::menuB()
{
    if(touch)
    {
        touch=false;
        float pos[2];

        pos[0]=-0.25f;
        pos[1]=0.33f;

        if(isCircleClicked(pos,fingerPos,b[0].r))
            return 1;
        else
        {
            pos[0]=0.25f;
            pos[1]=0.33f;
            if(isCircleClicked(pos,fingerPos,b[1].r))
                return 2;
            else
            {
                pos[0]=0.25f;
                pos[1]=-0.33f;
                if(isCircleClicked(pos,fingerPos,b[2].r))
                    return 3;
                else
                {
                    pos[0]=-0.25f;
                    pos[1]=-0.33f;
                    if(isCircleClicked(pos,fingerPos,b[3].r))
                        return 4;
                }
            }
        }
    }
    return 0;
}

int BouncyBallPc::menuW()
{
    if(touch)
    {
        touch=false;
        float pos[2];
        float a=0.20f;
        float b=0.3f;

        pos[0]=-0.25f;
        pos[1]=0.33f;
        if(isRectangleClicked(pos,fingerPos,a,b))
            return 2;
        else
        {
            pos[0]=0.25f;
            pos[1]=0.33f;
            if(isRectangleClicked(pos,fingerPos,a,b))
                return 6;
            else
            {
                pos[0]=0.25f;
                pos[1]=-0.33f;
                if(isRectangleClicked(pos,fingerPos,a,b))
                    return 7;
                else
                {
                    pos[0]=-0.25f;
                    pos[1]=-0.33f;
                    if(isRectangleClicked(pos,fingerPos,a,b))
                        return 8;
                }
            }
        }
    }
    return 0;
}
int BouncyBallPc::menuL()
{
    if(touch)
    {
        touch=false;
        float pos[2];
        float a=0.20f;
        float b=0.3f;

        pos[0]=-0.25f;
        pos[1]=0.33f;
        if(isRectangleClicked(pos,fingerPos,a,b))
            return 1;
        else
        {
            pos[0]=0.25f;
            pos[1]=0.33f;
            if(isRectangleClicked(pos,fingerPos,a,b))
                return 2;
            else
            {
                pos[0]=0.25f;
                pos[1]=-0.33f;
                if(isRectangleClicked(pos,fingerPos,a,b))
                    return 3;
                else
                {
                    pos[0]=-0.25f;
                    pos[1]=-0.33f;
                    if(isRectangleClicked(pos,fingerPos,a,b))
                        return 4;
                }
            }
        }
    }
    return 0;
}
int BouncyBallPc::menuM()
{
    if(touch)
    {
        touch=false;
        float pos[2];
        float a=0.2f;
        float b=0.3f;

        pos[0]=-0.25f;
        pos[1]=0.33f;
        if(isRectangleClicked(pos,fingerPos,a,b))
            return 1;
        else
        {
            pos[0]=0.0f;
            pos[1]=0.33f;
            if(isRectangleClicked(pos,fingerPos,a,b))
                return 2;
            else
            {
                pos[0]=0.25f;
                pos[1]=0.33f;
                if(isRectangleClicked(pos,fingerPos,a,b))
                    return 3;
                else
                {
                    pos[0]=-0.25f;
                    pos[1]=0.0f;
                    if(isRectangleClicked(pos,fingerPos,a,b))
                        return 4;
                    else
                    {
                        pos[0]=0.0f;
                        pos[1]=0.0f;
                        if(isRectangleClicked(pos,fingerPos,a,b))
                            return 5;
                        else
                        {
                            pos[0]=0.25f;
                            pos[1]=0.0f;
                            if(isRectangleClicked(pos,fingerPos,a,b))
                                return 6;
                            else
                            {
                                pos[0]=-0.25f;
                                pos[1]=-0.33f;
                                if(isRectangleClicked(pos,fingerPos,a,b))
                                    return 7;
                                else
                                {
                                    pos[0]=0.0f;
                                    pos[1]=-0.33f;
                                    if(isRectangleClicked(pos,fingerPos,a,b))
                                        return 8;
                                    else
                                    {
                                        pos[0]=-0.25f;
                                        pos[1]=-0.33f;
                                        if(isRectangleClicked(pos,fingerPos,a,b))
                                            return 9;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    return 0;
}

void BouncyBallPc::menu0()
{
    float pos[2];
    pos[0]=-0.25f;
    pos[1]=0.33f;
    emit addObject(m.idL[0],false,9,pos,0.0f);
    pos[0]=0.25f;
    pos[1]=0.33f;
    emit addObject(m.idL[1],false,10,pos,0.0f);
    pos[0]=0.25f;
    pos[1]=-0.33f;
    emit addObject(m.idL[2],false,11,pos,0.0f);
    pos[0]=-0.25f;
    pos[1]=-0.33f;
    emit addObject(m.idL[3],false,12,pos,0.0f);
}
void BouncyBallPc::menu1()
{
    float pos[2];
    pos[0]=-0.25f;
    pos[1]=0.33f;
    emit addObject(m.idB[0],false,1,pos,0.0f);
    pos[0]=0.25f;
    pos[1]=0.33f;
    emit addObject(m.idB[1],false,3,pos,0.0f);
    pos[0]=0.25f;
    pos[1]=-0.33f;
    emit addObject(m.idB[2],false,4,pos,0.0f);
    pos[0]=-0.25f;
    pos[1]=-0.33f;
    emit addObject(m.idB[3],false,5,pos,0.0f);
}
void BouncyBallPc::menu2()
{
    float pos[2];
    pos[0]=-0.25f;
    pos[1]=0.33f;
    emit addObject(m.idW[0],false,2,pos,0.0f);
    pos[0]=0.25f;
    pos[1]=0.33f;
    emit addObject(m.idW[1],false,6,pos,0.0f);
    pos[0]=0.25f;
    pos[1]=-0.33f;
    emit addObject(m.idW[2],false,7,pos,0.0f);
    pos[0]=-0.25f;
    pos[1]=-0.33f;
    emit addObject(m.idW[3],false,8,pos,0.0f);
}
void BouncyBallPc::menu3()
{
    float pos[2];
    pos[0]=-0.25f;
    pos[1]=0.33f;
    emit addObject(m.idM[0],false,13,pos,0.0f);
    pos[0]=0.0f;
    pos[1]=0.33f;
    emit addObject(m.idM[1],false,14,pos,0.0f);
    pos[0]=0.25f;
    pos[1]=0.33f;
    emit addObject(m.idM[2],false,15,pos,0.0f);

    pos[0]=-0.25f;
    pos[1]=0.0f;
    emit addObject(m.idM[3],false,16,pos,0.0f);
    pos[0]=0.0f;
    pos[1]=0.0f;
    emit addObject(m.idM[4],false,17,pos,0.0f);
    pos[0]=0.25f;
    pos[1]=0.0f;
    emit addObject(m.idM[5],false,18,pos,0.0f);

    pos[0]=-0.25f;
    pos[1]=-0.33f;
    emit addObject(m.idM[6],false,19,pos,0.0f);
    pos[0]=0.0f;
    pos[1]=-0.33f;
    emit addObject(m.idM[7],false,20,pos,0.0f);
    pos[0]=0.25f;
    pos[1]=-0.33f;
    emit addObject(m.idM[8],false,21,pos,0.0f);
}

void BouncyBallPc::menu()
{
    if(m.display)
    {
        if(m.lvl==-1)
        {
            menu0();
            m.lvl=0;
        }
        if(m.lvl==0)
            m.lvl=menuL();

        if(m.lvl>0 && m.ball==-1)
        {
            emit deleteObject(m.idL[0],9,false);
            emit deleteObject(m.idL[1],10,false);
            emit deleteObject(m.idL[2],11,false);
            emit deleteObject(m.idL[3],12,false);
            menu1();
            m.ball=0;
        }
        if(m.ball==0)
            m.ball=menuB();

        if(m.ball>0 && m.world==-1)
        {
            activeB=m.ball-1;
            emit deleteObject(m.idB[0],1,false);
            emit deleteObject(m.idB[1],3,false);
            emit deleteObject(m.idB[2],4,false);
            emit deleteObject(m.idB[3],5,false);
            menu2();
            m.world=0;
        }
        if(m.world==0)
            m.world=menuW();

        if(m.world>0 && m.mis==-1)
        {
            emit deleteObject(m.idW[0],2,false);
            emit deleteObject(m.idW[1],6,false);
            emit deleteObject(m.idW[2],7,false);
            emit deleteObject(m.idW[3],8,false);
            menu3();
            m.mis=0;
        }

        if(m.mis==0)
            m.mis=menuM();

        if(m.mis>0)
        {
            m.display=false;
            emit deleteObject(m.idM[0],13,false);
            emit deleteObject(m.idM[1],14,false);
            emit deleteObject(m.idM[2],15,false);
            emit deleteObject(m.idM[3],16,false);
            emit deleteObject(m.idM[4],17,false);
            emit deleteObject(m.idM[5],18,false);
            emit deleteObject(m.idM[6],19,false);
            emit deleteObject(m.idM[7],20,false);
            emit deleteObject(m.idM[8],21,false);
            emit deleteObject(m.idM[9],22,false);
            startLvl();
        }
    }
}

void BouncyBallPc::startLvl()
{
    float gravity[2];
    switch(m.world)
    {
    case 2:
    {
        gravity[0]=0.0f;
        gravity[1]=-9.81f;
        break;
    }
    case 6:
    {
        gravity[0]=0.0f;
        gravity[1]=-2.0f;
        break;
    }
    case 7:
    {
        gravity[0]=0.0f;
        gravity[1]=-20.0f;
        break;
    }
    case 8:
    {
        gravity[0]=6.0f;
        gravity[1]=-9.81f;
        break;
    }
    }
    emit setGravity(gravity);
    emit setSpeed(0.25f*m.lvl);
    float pos[2];
    pos[0]=0.0f;
    pos[1]=0.0f;
    float v[2];
    v[0]=-2.8f;
    v[1]=5.0f;
    emit addObject(id,true,b[activeB].modelN,pos,0.0f,v,0.0f,b[activeB].mass,b[activeB].friction,b[activeB].restutition,true,b[activeB].density);    
}
void BouncyBallPc::gameStep()
{
    switch(m.mis)
    {
    case 0:
    {
        break;
    }
    case 1:
    {
        mis1();
        break;
    }
    case 2:
    {
        mis2();
        break;
    }
    case 3:
    {
        mis3();
        break;
    }
    case 4:
    {
        mis4();
        break;
    }
    case 5:
    {
        mis5();
        break;
    }
    case 6:
    {
        mis6();
        break;
    }
    case 7:
    {
        mis7();
        break;
    }
    case 8:
    {
        mis8();
        break;
    }
    case 9:
    {
        mis9();
        break;
    }
    }
}

void BouncyBallPc::mis1()
{
    static int points;
    if(counter%40==0)
        points+=50;
    int idC;
    emit contact(id,b[activeB].modelN,idC);
    if(idC==-1)
    {
        endMis();
    }
}
void BouncyBallPc::mis2()
{

}
void BouncyBallPc::mis3()
{

}
void BouncyBallPc::mis4()
{

}
void BouncyBallPc::mis5()
{

}
void BouncyBallPc::mis6()
{

}
void BouncyBallPc::mis7()
{

}
void BouncyBallPc::mis8()
{

}
void BouncyBallPc::mis9()
{

}

void BouncyBallPc::endMis()
{
    m.ball=-1;
    m.world=-1;
    m.lvl=-1;
    m.mis=-1;
    emit deleteObject(id,b[activeB].modelN,true);
    m.display=true;
}
