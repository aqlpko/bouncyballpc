#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QTime>
#include <QMatrix4x4>
#include <cmath>
#include <QtOpenGL/QGLWidget>
#include <QGuiApplication>
#include <QScreen>
#include <QSize>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    float gravity[2];

    gravity[0]=0.0f;
    gravity[1]=-9.81f;
    screenDimension[0]=480;
    screenDimension[1]=640;
    int fps=40;
    modelNumber=22;
    sceneDimension[0]=-0.5;//left,right,bottom,top,near,far,angle
    sceneDimension[1]=0.5;

    float aspect=(float)screenDimension[0]/screenDimension[1];
    sceneDimension[2]=sceneDimension[0]/aspect;
    sceneDimension[3]=sceneDimension[1]/aspect;
    sceneDimension[4]=3;
    sceneDimension[5]=7;
    sceneDimension[6]=0;

    list=new QList<viewMatrix*>[modelNumber];
    graphicsObject=new int[modelNumber];
    id=0;
    for(int i=0;i<modelNumber;i++)
        graphicsObject[i]=0;

    dt=1.0/fps;

    float dimension[2];
    dimension[0]=sceneDimension[1];
    dimension[1]=sceneDimension[3];
    physics=new PhysicsEngine(dt,modelNumber,gravity,dimension,list,graphicsObject);

    data=new VertexData(modelNumber);
    ui->render->initializeValues(screenDimension,sceneDimension,modelNumber,data->getVertices(),data->getIndices(),data->getIndicesNumber(),list);

    game=new BouncyBallPc(dt);
    connect(game,SIGNAL(newFrame(float*,bool&)),this,SLOT(updateGame(float*,bool&)));
    connect(game,SIGNAL(addObject(int&,bool,int,float*,float,float*,float,float,float,float,bool,float)),this,SLOT(addObject(int&,bool,int,float*,float,float*,float,float,float,float,bool,float)));
    connect(game,SIGNAL(deleteObject(int,int,bool)),this,SLOT(deleteObject(int,int,bool)));
    connect(game,SIGNAL(setGravity(float*)),this,SLOT(setGravity(float*)));
    connect(game,SIGNAL(setForce(int,int,float*,float*)),this,SLOT(setForce(int,int,float*,float*)));
    connect(game,SIGNAL(getObjectPos(int,int,float*)),this,SLOT(getPos(int,int,float*)));
    connect(game,SIGNAL(setSpeed(float)),this,SLOT(setSpeed(float)));
    connect(game,SIGNAL(contact(int,int,int&)),this,SLOT(contact(int,int,int&)));
    game->start();
}

MainWindow::~MainWindow()
{
    delete ui;
}
void MainWindow::updateGame(float *fingerPos_, bool &isClicked_)
{
    physics->updatePhysics();
    ui->render->updateGraphics();
    if(isClicked)
    {
        fingerPos_[0]=fingerPos[0];
        fingerPos_[1]=fingerPos[1];
        isClicked=false;
        isClicked_=true;
    }
    else
        isClicked_=false;
}
void MainWindow::setGraphicsObject()
{

}

void MainWindow::mousePressEvent(QMouseEvent *event)
{
    isClicked=true;
    fingerPos[0]=(((float)event->x())/screenDimension[0])*sceneDimension[1]*2-sceneDimension[1];
    fingerPos[1]=(((float)screenDimension[1]-event->y())/screenDimension[1])*sceneDimension[3]*2-sceneDimension[3];
}

void MainWindow::addObject(int &id, bool isPhysical, int modelN, float* pos, float angle, float* v, float omega, float mass, float friction, float restutition, bool isDynamic, float density)
{

    int newId=getId(modelN);
    id=newId;

    if(isPhysical)
    {
        viewMatrix* newObject=new viewMatrix;
        newObject->m=new QMatrix4x4;
        newObject->id=newId;
        list[modelN].append(newObject);
        float* d1=data->getVertices()[modelN];
        GLushort* d2=data->getIndices()[modelN];
        int n=data->getIndicesNumber()[modelN];
        physics->createObject(modelN,d1,d2,n,pos,angle,v,omega,mass,friction,restutition,isDynamic,density,newId);
    }
    else
    {
        viewMatrix* newObject=new viewMatrix;
        newObject->m=new QMatrix4x4;
        newObject->m->setToIdentity();
        newObject->m->translate(pos[0],pos[1]);
        newObject->m->rotate(angle,0,0,1);
        newObject->id=newId;
        list[modelN].insert(graphicsObject[modelN],newObject);
        graphicsObject[modelN]++;
    }
}

void MainWindow::getPos(int id, int modelN,float* pos)
{
    physics->getPos(id,modelN,pos);
}

int MainWindow::getId(int modelN)
{
    int n=list[modelN].size();
    for(int i=0;i<n;i++)
    {
        if(list[modelN][i]->id==0)
        {
            id++;
            return id;
        }
        id++;
        return id;
    }
}

void MainWindow::deleteObject(int id,int modelN,bool isPhysical)
{
    if(isPhysical)
    {
        if(physics->deleteObject(id,modelN))
        {
            delete list[modelN].last()->m;
            list[modelN].removeLast();
        }
    }
    else
    {
        for(int i=0;i<graphicsObject[modelN];i++)
            if(list[modelN][i]->id==id)
            {
                delete list[modelN][i];
                list[modelN].removeAt(i);
                graphicsObject[modelN]--;
            }
    }
}

void MainWindow::setGravity(float *gravity)
{
    physics->setGravity(gravity);
}
void MainWindow::setForce(int id,int modelN, float *force,float *pos)
{
    physics->setForce(id,modelN,force,pos);
}
void MainWindow::setSpeed(float speed)
{
    physics->setSpeed(speed);
}
void MainWindow::contact(int id, int modelN, int &idC)
{
    physics->getContact(id,modelN,idC);
}
