#include "vertexdata.h"

VertexData::VertexData(int n_)
{
    n=n_;
    verticesNumber=new GLsizeiptr[n];
    indicesNumber=new GLuint[n];
    verticesPNumber=new GLsizeiptr[n];
    vertices=new GLfloat*[n];
    indices=new GLushort*[n];
    verticesP=new GLfloat*[n];
#include "zasoby\indexdata0.h"
#include "zasoby\vertexdata0.h"
#include "zasoby\indexdata1.h"
#include "zasoby\vertexdata1.h"
#include "zasoby\indexdata2.h"
#include "zasoby\vertexdata2.h"
#include "zasoby\indexdata3.h"
#include "zasoby\vertexdata3.h"
#include "zasoby\indexdata4.h"
#include "zasoby\vertexdata4.h"
#include "zasoby\indexdata5.h"
#include "zasoby\vertexdata5.h"
#include "zasoby\indexdata6.h"
#include "zasoby\vertexdata6.h"
#include "zasoby\indexdata7.h"
#include "zasoby\vertexdata7.h"
#include "zasoby\indexdata8.h"
#include "zasoby\vertexdata8.h"
#include "zasoby\indexdata9.h"
#include "zasoby\vertexdata9.h"
#include "zasoby\indexdata10.h"
#include "zasoby\vertexdata10.h"
#include "zasoby\indexdata11.h"
#include "zasoby\vertexdata11.h"
#include "zasoby\indexdata12.h"
#include "zasoby\vertexdata12.h"
#include "zasoby\indexdata13.h"
#include "zasoby\vertexdata13.h"
#include "zasoby\indexdata14.h"
#include "zasoby\vertexdata14.h"
#include "zasoby\indexdata15.h"
#include "zasoby\vertexdata15.h"
#include "zasoby\indexdata16.h"
#include "zasoby\vertexdata16.h"
#include "zasoby\indexdata17.h"
#include "zasoby\vertexdata17.h"
#include "zasoby\indexdata18.h"
#include "zasoby\vertexdata18.h"
#include "zasoby\indexdata19.h"
#include "zasoby\vertexdata19.h"
#include "zasoby\indexdata20.h"
#include "zasoby\vertexdata20.h"
#include "zasoby\indexdata21.h"
#include "zasoby\vertexdata21.h"
}

VertexData::~VertexData(){}

GLfloat** VertexData::getVertices()
{
    return vertices;
}
GLushort** VertexData::getIndices()
{
    return indices;
}
GLuint* VertexData::getIndicesNumber()
{
    return indicesNumber;
}
GLsizeiptr* VertexData::getVerticesNumber()
{
    return verticesNumber;
}
GLfloat** VertexData::getVerticesP()
{
    return verticesP;
}
GLsizeiptr* VertexData::getVerticesPNumber()
{
    return verticesPNumber;
}
