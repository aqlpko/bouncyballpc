#include "graphicsengine.h"
#include <cmath>
MyGLWidget::MyGLWidget(QWidget *parent) :
    QGLWidget(parent)
{
}

void MyGLWidget::initializeGL()
{
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_TEXTURE_2D);
    glEnable(GL_CULL_FACE);
    glEnable(GL_DITHER);
    glClearColor(0.5,0.5,0,1);
    loadTextures();
}

void MyGLWidget::resizeGL(int w,int h)
{
    projectionMatrix.setToIdentity();
    GLfloat aspect=(GLfloat)screenDimension[0]/screenDimension[1];
    if(sceneDimension[0]==0)
        projectionMatrix.perspective(sceneDimension[6],aspect,sceneDimension[4],sceneDimension[5]);
    else
        projectionMatrix.ortho(sceneDimension[0],sceneDimension[1],sceneDimension[2],sceneDimension[3],sceneDimension[4],sceneDimension[5]);
    glViewport(0,0,w,h);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glMatrixMode(GL_MODELVIEW);
}

void MyGLWidget::paintGL()
{
    float x,y;
    int n=0;
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    for(GLint i=0;i<modelNumber;i++)
    {
        glBindTexture(GL_TEXTURE_2D,tex[i]);
        n=list[i].size();
        for(GLint j=0;j<n;j++)
        {
            viewProjectionMatrix=projectionMatrix*constantMatrix**list[i][j]->m;
            glLoadMatrixf(viewProjectionMatrix.data());
            glBegin(GL_TRIANGLES);
            for(int k=0;k<indicesNumber[i];k++)
            {
               glTexCoord2f(vertices[i][indices[i][k]*4+2],vertices[i][indices[i][k]*4+3]);
               glVertex2f(vertices[i][indices[i][k]*4],vertices[i][indices[i][k]*4+1]);
            }
            glEnd();
        }
    }
}

void MyGLWidget::initializeValues(int *screenDimensionValue, float *sceneDimensionValue, int modelNumberValue, GLfloat **vertices_, GLushort **indices_, GLuint* indicesNumber_,QList<viewMatrix*> *list_)
{
    screenDimension[0]=screenDimensionValue[0];
    screenDimension[1]=screenDimensionValue[1];
    sceneDimension[0]=sceneDimensionValue[0];
    sceneDimension[1]=sceneDimensionValue[1];
    sceneDimension[2]=sceneDimensionValue[2];
    sceneDimension[3]=sceneDimensionValue[3];
    sceneDimension[4]=sceneDimensionValue[4];
    sceneDimension[5]=sceneDimensionValue[5];
    sceneDimension[6]=sceneDimensionValue[6];
    list=list_;
    modelNumber=modelNumberValue;
    indices=indices_;
    vertices=vertices_;
    indicesNumber=indicesNumber_;
    constantMatrix.setToIdentity();
    constantMatrix.translate(0,0,-0.5*(sceneDimension[4]+sceneDimension[5]));
    objectNumber=new GLint[modelNumber];
    for(GLint i=0;i<modelNumber;i++)
        objectNumber[i]=0;
}

void MyGLWidget::updateGraphics()
{
   // viewMatrix=viewMatrixValue;
    //objectNumber=objectNumberValue;
    updateGL();
}

void MyGLWidget::loadTextures()
{
    glEnable(GL_TEXTURE_2D);
    tex=new GLuint[modelNumber];
    glGenTextures(modelNumber,tex);
    QImage plik;
    int w,h;
    for(GLint i=0;i<modelNumber;i++)
    {
        plik.load(":/textures/zasoby/texture"+QString::number(i)+".png");
        plik=QGLWidget::convertToGLFormat(plik);
        w=plik.width();
        h=plik.height();
        glBindTexture(GL_TEXTURE_2D,tex[i]);
        glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_NEAREST);
        glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_NEAREST);
        glTexImage2D(GL_TEXTURE_2D,0,GL_RGBA,w,h,0,GL_RGBA,GL_UNSIGNED_BYTE,(GLvoid*) plik.bits());
    }
}
