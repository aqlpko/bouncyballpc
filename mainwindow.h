#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QVector2D>
#include <QMouseEvent>
#include "physicsengine.h"
#include <QPushButton>
#include "vertexdata.h"
#include "bouncyballpc.h"
#include "viewMatrix.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    void mousePressEvent(QMouseEvent *event);
    int getId(int modelN);

public slots:
    void updateGame(float* fingerPos_,bool &isClicked_);
    void setGraphicsObject();
    void addObject(int &id,bool isPhysical, int modelN, float* pos, float angle, float* v=NULL, float omega=0.0f, float mass=0.0f, float friction=0.0f, float restutition=0.0f, bool isDynamic=true, float density=0.0f);
    void deleteObject(int id,int modelN,bool isPhysical);
    void setGravity(float* gravity);
    void setForce(int id,int modelN, float *force,float *pos);
    void getPos(int id, int modelN, float *pos);
    void setSpeed(float speed);
    void contact(int id,int modelN,int &idC);

private:
    Ui::MainWindow *ui;
    float dt;
    int screenDimension[2];
    float sceneDimension[7];
    float fingerPos[2];
    bool isClicked;
    int modelNumber, *graphicsObject;
    QList<viewMatrix*> *list;
    PhysicsEngine *physics;
    VertexData *data;
    BouncyBallPc *game;
    int id;
};


#endif // MAINWINDOW_H
