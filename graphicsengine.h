#ifndef MYGLWIDGET_H
#define MYGLWIDGET_H

#include <QGLWidget>
#include <QMatrix4x4>
#include <QImage>
#include "viewMatrix.h"

class MyGLWidget : public QGLWidget
{
    Q_OBJECT

protected:
    void initializeGL();
    void resizeGL(int w, int h);
    void paintGL();
    QMatrix4x4 projectionMatrix,viewProjectionMatrix,constantMatrix;
    QList<viewMatrix*> *list;
    GLint* objectNumber,modelNumber;
    GLfloat sceneDimension[7];
    GLuint screenDimension[2];
    GLuint *tex;
    GLfloat **vertices;
    GLushort **indices;
    GLuint* indicesNumber;

public:
    explicit MyGLWidget(QWidget *parent = 0);
    void initializeValues(int *screenDimensionValue, float *sceneDimensionValue, int modelNumberValue, GLfloat **vertices_, GLushort **indices_,GLuint* indicesNumber_,QList<viewMatrix*> *list_);
    void updateGraphics();
    void loadTextures();


signals:

public slots:

};

#endif // MYGLWIDGET_H
