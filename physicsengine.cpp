#include "physicsengine.h"

PhysicsEngine::PhysicsEngine(float dt_, int modelNumber_, float *gravity_, float *dimension, QList<viewMatrix *> *listM_, int *graphicsObject_)
{
    dt=dt_;
    modelNumber=modelNumber_;
    graphicsObject=graphicsObject_;
    objectNumber=new int[modelNumber];
    list=new QList<object*>[modelNumber];
    listM=listM_;
    gravity=b2Vec2(gravity_[0],gravity_[1]);
    createWorld(dimension);
    world->SetContactListener(&contactListener);
}
PhysicsEngine::~PhysicsEngine()
{

}

void PhysicsEngine::createWorld(float *dimension)
{
    world=new b2World(gravity);

    groundBodyDef.position.Set(0.0f,dimension[1]*-1.0f-0.1f);
    groundBody=world->CreateBody(&groundBodyDef);
    groundBox.SetAsBox(dimension[0],0.1f);
    groundBody->CreateFixture(&groundBox,0.0f);

    groundBodyDef.position.Set(dimension[0]*-1.0f-0.1f,0.0f);
    groundBody=world->CreateBody(&groundBodyDef);
    groundBox.SetAsBox(0.1f,dimension[1]);
    groundBody->CreateFixture(&groundBox,0.0f);

    groundBodyDef.position.Set(dimension[0]+0.1f,0.0f);
    groundBody=world->CreateBody(&groundBodyDef);
    groundBox.SetAsBox(0.1f,dimension[1]);
    groundBody->CreateFixture(&groundBox,0.0f);

    groundBodyDef.position.Set(0.0f,dimension[1]+0.1f);
    groundBody=world->CreateBody(&groundBodyDef);
    groundBox.SetAsBox(dimension[0],0.1f);
    groundBody->CreateFixture(&groundBox,0.0f);

    velocityIterations=8;
    positionIterations=3;
}
void PhysicsEngine::createObject(int modelN, float *data1, unsigned short* data2, int nIndices, float* pos, float angle, float* v, float omega, float mass, float friction, float restutition, bool isDynamic, float density, int id_)
{
    object* newObject=new object;
    newObject->id=id_;
    newObject->bodyDef=new b2BodyDef;
    if(isDynamic)
        newObject->bodyDef->type=b2_dynamicBody;
    else
        newObject->bodyDef->type=b2_staticBody;
    newObject->bodyDef->position.Set(pos[0],pos[1]);
    newObject->bodyDef->angle=angle*0.01744f;
    newObject->body=world->CreateBody(newObject->bodyDef);

    b2Vec2 vertices[3];

    for(int j=0;j<nIndices;j+=3)
    {
        for(int i=0;i<3;i++)
            vertices[i].Set(data1[data2[j+i]*4],data1[(data2[j+i]*4)+1]);

        b2PolygonShape shape;
        shape.Set(vertices,3);
        newObject->fixtureDef=new b2FixtureDef;
        newObject->fixtureDef->shape=&shape;
        newObject->fixtureDef->friction=friction;
        newObject->fixtureDef->restitution=restutition;
        newObject->fixtureDef->density=density;
        newObject->body->CreateFixture(newObject->fixtureDef);
    }

    newObject->body->SetLinearVelocity(b2Vec2(v[0],v[1]));
    newObject->body->SetAngularVelocity(omega);
    newObject->body->SetUserData(newObject);
    list[modelN].append(newObject);
}
void PhysicsEngine::updatePhysics()
{
    int n,m;
    b2Vec2 pos;
    world->Step(dt,velocityIterations,positionIterations);
    for(int i=0;i<modelNumber;i++)
    {
        n=list[i].size()+graphicsObject[i];
        for(int j=graphicsObject[i];j<n;j++)
        {
            m=j-graphicsObject[i];
            pos=list[i][m]->body->GetPosition();
            float x=pos.x;
            float y=pos.y;
            listM[i][j]->m->setToIdentity();
            listM[i][j]->m->translate(pos.x,pos.y);
            listM[i][j]->m->rotate(list[i][m]->body->GetAngle()*57.32f,0,0,1);
        }
    }
}
void PhysicsEngine::setForce(int id, int modelN,float *force_,float* pos_)
{    
    int n=list[modelN].size();
    for(int i=0;i<n;i++)
    {
        if(list[modelN][i]->id==id)
        {
            b2Vec2 force;
            force.Set(force_[0],force_[1]);
            b2Vec2 pos;
            pos.Set(pos_[0],pos_[1]);
            list[modelN][i]->body->ApplyForce(force,pos,true);
            break;
        }
    }
}

void PhysicsEngine::getPos(int id, int modelN,float* pos_)
{
    int n=list[modelN].size();
    for(int i=0;i<n;i++)
    {
        if(list[modelN][i]->id==id)
        {
            b2Vec2 pos=list[modelN][i]->body->GetPosition();
            pos_[0]=pos.x;
            pos_[1]=pos.y;
        }
    }
}

bool PhysicsEngine::deleteObject(int id,int modelN)
{
    int n=list[modelN].size();
    for(int i=0;i<n;i++)
    {
        if(list[modelN][i]->id==id)
        {
           delete list[modelN][i];
           return true;
        }
    }
    return false;
}

void PhysicsEngine::setGravity(float *gravity_)
{
    gravity.Set(gravity_[0],gravity_[1]);
    world->SetGravity(gravity);
}
void PhysicsEngine::setSpeed(float speed)
{
    gameSpeed=speed;
    dt*=gameSpeed;
}
void PhysicsEngine::getContact(int id, int modelN, int &idC)
{
    int n=list[modelN].size();
    for(int i=0;i<n;i++)
    {
        if(list[modelN][i]->id==id)
            idC=list[modelN][i]->contact;
    }
}
