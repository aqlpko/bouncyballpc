#ifndef VIEWMATRIX_H
#define VIEWMATRIX_H

#include <QMatrix4x4>

struct viewMatrix
{
    QMatrix4x4* m;
    int id;
};

#endif // VIEWMATRIX_H
