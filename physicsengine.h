#ifndef PHYSICSENGINE_H
#define PHYSICSENGINE_H

#include "Box2D/Box2D.h"
#include <QList>
#include <QMatrix4x4>
#include "viewMatrix.h"

struct object
{
    b2BodyDef *bodyDef;
    b2FixtureDef *fixtureDef;
    b2Body *body;
    int id;
    int contact;
};

class Contact : public b2ContactListener
{
    void BeginContact(b2Contact* contact)
    {
         void* bodyUserDataA = contact->GetFixtureA()->GetBody()->GetUserData();
         void* bodyUserDataB = contact->GetFixtureB()->GetBody()->GetUserData();
         if(!bodyUserDataA)
         {
            static_cast<object*>(bodyUserDataB)->contact=-1;
         }
         else
         {
             if(!bodyUserDataB)
             {
                 static_cast<object*>(bodyUserDataA)->contact=-1;
             }
             else
             {
                 static_cast<object*>(bodyUserDataA)->contact=static_cast<object*>(bodyUserDataB)->id;
                 static_cast<object*>(bodyUserDataB)->contact=static_cast<object*>(bodyUserDataA)->id;
             }
         }
    }

    void EndContact(b2Contact* contact)
    {
        void* bodyUserDataA = contact->GetFixtureA()->GetBody()->GetUserData();
        void* bodyUserDataB = contact->GetFixtureB()->GetBody()->GetUserData();
        if(bodyUserDataA)
          static_cast<object*>(bodyUserDataA)->contact=-2;

        if(bodyUserDataB)
          static_cast<object*>(bodyUserDataB)->contact=-2;
    }
};

class PhysicsEngine
{
    b2World *world;
    b2Vec2 gravity;

    b2BodyDef groundBodyDef;
    b2Body *groundBody;
    b2PolygonShape groundBox;

    float32 dt;
    int32 velocityIterations;
    int32 positionIterations;

    float gameSpeed;

    int modelNumber;
    int* objectNumber;

    QList<object*> *list;
    QList<viewMatrix*> *listM;
    int *graphicsObject;
    Contact contactListener;

public:
    PhysicsEngine(float dt_, int modelNumber_, float *gravity_, float* dimension,QList<viewMatrix*> *listM_,int *graphicsObject_);
    ~PhysicsEngine();
    void createWorld(float* dimension);
    void updatePhysics();
    void createObject(int modelN,float *data1,unsigned short* data2,int nIndices, float* pos, float angle, float* v, float omega, float mass, float friction, float restutition, bool isDynamic, float density,int id_);
    void setForce(int id, int modelN, float *force_, float *pos_);
    void getPos(int id, int modelN, float* pos_);
    bool deleteObject(int id,int modelN);
    void setGravity(float* gravity_);
    void setSpeed(float speed);
    void getContact(int id,int modelN,int &idC);
};

#endif // PHYSICSENGINE_H
