#ifndef BOUNCYBALLPC_H
#define BOUNCYBALLPC_H

#include <QTimer>

class BouncyBallPc:public QObject
{
    Q_OBJECT
    int counter;
    QTimer update;
    int id;
    float fingerPos[2];
    float lastFingerPos[2];
    bool touch;
    int activeB;

    struct menu
    {
        int lvl;
        int mis;
        bool display;
        int ball;
        int world;
        int idB[4];
        int idW[4];
        int idL[4];
        int idM[9];
    };

    menu m;

    struct ball
    {
        float mass,friction,restutition,density;
        float r;
        int modelN;
    };

    ball b[4];

public:
    BouncyBallPc(float dt);
    ~BouncyBallPc();
    void start();
    void setGraphicsObject();
    void menu2();
    void menu1();
    void menu0();
    void menu3();
    void menu();
    int menuB();
    int menuW();
    int menuL();
    int menuM();
    void startLvl();
    bool isCircleClicked(float *pos1, float *pos2, float r);
    bool isRectangleClicked(float *pos1, float *pos2, float a, float b);
    void setForce();
    void gameStep();
    void endMis();
    void mis1();
    void mis2();
    void mis3();
    void mis4();
    void mis5();
    void mis6();
    void mis7();
    void mis8();
    void mis9();

public slots:
    void step();
signals:
    void newFrame(float *fingerPos,bool &isClicked);
    void addObject(int &id,bool isPhysical,int modelN,float* pos,float angle,float* v=NULL,float omega=0.0f,float mass=0.0f,float friction=0.0f,float restutition=0.0f,bool isDynamic=true,float density=0.0f);
    void deleteObject(int id,int modelN,bool isPhysical);
    void setGravity(float *gravity);
    void setForce(int id,int modelN, float *force,float *pos);
    void getObjectPos(int id,int modelN,float* pos);
    void setSpeed(float speed);
    void contact(int id,int modelN,int &idC);
};

#endif // BOUNCYBALLPC_H
